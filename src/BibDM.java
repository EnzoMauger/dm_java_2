import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer min = null;
        for (Integer i : liste) {
          if (min == null || i < min) { min = i;}
        }
        return min;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean plusPetit = true;
        int i = 0;
        int longueurListe = liste.size();
        while (plusPetit && i < longueurListe) {
          if (liste.get(i).compareTo(valeur) <= 0) { plusPetit = false;}
          i++;
        }
        return plusPetit;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listeRes = new ArrayList<>();
        int i, j, longueurL1, longueurL2;
        Integer valeurComparaison = null; //Valeur qui servira à stocker la valeur des compareTo à chaque tour de boucle
        T valeurAajouter = null;
        i = 0;
        j = 0;
        longueurL1 = liste1.size();
        longueurL2 = liste2.size();
        while (i < longueurL1 && j < longueurL2) {
          valeurComparaison = liste1.get(i).compareTo(liste2.get(j));
          if (valeurComparaison > 0) { j++;} else {
            if (valeurComparaison < 0) { i++;} else {
              valeurAajouter = liste1.get(i);
              if (!listeRes.contains(valeurAajouter)) { listeRes.add(valeurAajouter);}
              i++;
              j++;
            }
          }
        }
        return listeRes;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeMots = new ArrayList<>();
        String motCourant = new String();
        int i = 0;
        Character espace = " ".charAt(0);
        Character charActuel = null;
        while (i < texte.length()) {
          charActuel = texte.charAt(i);
          if (charActuel.equals(espace) && !motCourant.equals("")) {
            listeMots.add(motCourant);
            motCourant = "";
          } else { if (!charActuel.equals(espace)) motCourant += charActuel;}
          i++;
        }
        if (!motCourant.equals("")) {
          listeMots.add(motCourant);
        }
        return listeMots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        List<String> listeTexte = decoupe(texte);
        Map<String, Integer> dicoMots = new HashMap<>();
        Integer valUn = new Integer(1);
        for (String mot : listeTexte) {
          if (dicoMots.containsKey(mot)) {
            dicoMots.put(mot, dicoMots.get(mot) + valUn);
          } else {
            dicoMots.put(mot, valUn);
          }
        }
        String motMajoritaire = null;
        int occurenceMax = 0;
        Integer occurenceActuelle = null;
        for (String motCourant : dicoMots.keySet()) {
          occurenceActuelle = dicoMots.get(motCourant);
          if (motMajoritaire == null || occurenceActuelle > occurenceMax ) {
            occurenceMax = occurenceActuelle;
            motMajoritaire = motCourant;
          } else {
            if (occurenceActuelle == occurenceMax) {
              if (motCourant.compareTo(motMajoritaire) < 0) {
                motMajoritaire = motCourant;
              }
            }
          }
        }
        return motMajoritaire;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;
        boolean pbOuverture = false;
        int longueurChaine = chaine.length();
        Character parGauche = "(".charAt(0);
        Character parDroite = ")".charAt(0);
        Character charActuel = null;
        for (int i = 0; i < longueurChaine; i++) {
          charActuel = chaine.charAt(i);
          if (charActuel.equals(parGauche)) {
            cpt++;
          } else {
            if (charActuel.equals(parDroite)) {
              if (cpt <= 0) { pbOuverture = true;}
              else { cpt--;}
            }
          }
        }
        return cpt == 0 && !pbOuverture;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      int cptPar = 0;
      int cptCro = 0;
      boolean pbOuverture = false;
      int longueurChaine = chaine.length();
      Character parGauche = "(".charAt(0);
      Character parDroite = ")".charAt(0);
      Character croGauche = "[".charAt(0);
      Character croDroite = "]".charAt(0);
      Character charActuel = null;
      for (int i = 0; i < longueurChaine; i++) {
        charActuel = chaine.charAt(i);
        if (charActuel.equals(parGauche)) {
          cptPar++;
        } else {
          if (charActuel.equals(croGauche)) {
            cptCro++;
          } else {
            if (charActuel.equals(parDroite)) {
              if (cptPar <= 0) { pbOuverture = true;} else { cptPar--;}
            } else {
              if (charActuel.equals(croDroite)) {
                if (cptCro <= 0) { pbOuverture = true;} else { cptCro--;}
              }
            }
          }
        }
      }
      return (cptPar == 0 & cptCro == 0) && !pbOuverture;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int i = 0;
        int j = liste.size();
        Integer milieu = null;
        boolean trouve = false;
        Integer valCourante = null;
        while (i < j && !trouve) {
          milieu = (i + j) / 2;
          valCourante = liste.get(milieu);
          if (valCourante < valeur) { i = milieu;}
          else {
            if (valCourante > valeur) { j = milieu;}
            else { trouve = true;}
          }
        }
        return trouve;
    }



}
